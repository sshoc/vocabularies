# Vocabularies used for SSHOC projects

Most of the vocabularies are used by the SSH Open Marketplace. There are also vocabularies that are used in other projects: conversion-hub, training-discovery-toolkit

The repository was moved to https://github.com/SSHOC/vocabularies and restructured. Please refer to the new location, this location is deprecated.
